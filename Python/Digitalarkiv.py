﻿from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 
time.sleep (2)

#driver = webdriver.Chrome ()
driver = webdriver.Chrome()
driver.get("https://www.digitalarkivet.no/") 
time.sleep (2)

element=driver.find_element_by_link_text("Avansert personsøk")
element.click()
time.sleep (2)

#Finne navn:
element= driver.find_element_by_name("firstname")
element.send_keys("Karoline")
element= driver.find_element_by_name("lastname")
element.send_keys("Nilsen")
time.sleep (2)

#Finne fødselsår fra
element= driver.find_element_by_name("birth_year_from")
element.send_keys("1857")
time.sleep (2)

#Finne fødselsår til
element= driver.find_element_by_name("birth_year_to")
element.send_keys("1857")
time.sleep (2)

#Finne fødselsår til
element= driver.find_element_by_name("birth_date")
element.send_keys("09-01")
time.sleep (2)

#Finne fødselssted
element= driver.find_element_by_name("birth_place")
element.send_keys("Aremark")
time.sleep (2)

#Finne søkeknappen
element.send_keys(Keys.RETURN)
time.sleep (2)

#Trykke på søkeresultat
element= driver.find_element_by_class_name("block-link")
element.send_keys(Keys.RETURN)
time.sleep (2)

#Finne antall som bor i leiligheten:
element = driver.find_element_by_link_text("10")
element.send_keys(Keys.RETURN)
time.sleep (2)

#Test
list= driver.find_elements_by_class_name("data-item")

#Finne antall som bor i leiligheten:
print("antall beboere")
print(len(list)) 
time.sleep (2)

element = driver.find_element_by_link_text("0029 Storgate 33")
element.send_keys(Keys.RETURN)
time.sleep (2)

#Finne husttype                                       
HusType = driver.find_elements_by_xpath("//div[@class='col-xs-12 col-md-6 ssp-semibold' and contains(text(), '')]")
HusType2 = ((HusType)[4].text )
print('Hustype: ' + str(HusType2))

